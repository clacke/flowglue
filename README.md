# Flowglue

## The simplest multi-process Flow-Based-Programming-like framework that could possibly work.

What is the most fundamental almost-FBP implementation that is available almost anywhere? Unix pipes.

What is the scrappiest glue for unix pipes that has an event loop, decent serialization and a GUI? Tcl.

This is a Tcl [FBP-like](http://www.jpaulmorrison.com/fbp/noflo.html), but it can be true FBP if you treat it right.

## Limitations

 - There is no graph-composition-time checking that edges go to ports that actually exist, as there is no protocol for
   enumerating ports of managed processes.
 - There is no concept of an array port. But as we are so loose about ports anyway, you can emulate them by just
   calling your ports `<portname>#<arrayindex>`, or `<portname>#<elementname>` if you like named array elements.

## Process interface

The interface between processes is as follows:

 - We call Information Packages (IP) "messages".
 - A process representing a node is the managed process.
 - A process representing the graph containing the node is the managing process. It starts the managed process.
 - A managed process will read messages on all its readable file descriptors. If it's not going to read on one, it needs
   to close it, so the managing process knows not to send messages there.
 - Each message is a [netstring](https://cr.yp.to/proto/netstrings.txt)
   containing two netstrings: First the input port name, then the message body.
 - A message-like so short that it doesn't fit the second netstring is not a message, it's a non-message, which lets the
   process know in advance that this port has been connected. This can be relevant for nodes that want to
   enumerate the elements of their emulated array ports.
 - The managed process will write messages on any of its writable ports except stderr, using a similarly nested
   netstring with an output port name and a message body.
 - A managing process that wants to do true FBP needs to provide enough FDs for the managed process, and send the
   messages for at most one port on each FD.
 - A managed process that wants to do true FBP needs to send messages for at most one port on each FD.

## Internal Tcl interface

TBD
